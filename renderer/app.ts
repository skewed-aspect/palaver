// ---------------------------------------------------------------------------------------------------------------------
// Renderer Process
// ---------------------------------------------------------------------------------------------------------------------

import Vue from 'vue';
import VueRouter from 'vue-router';

// Bootstrap Vue
import BootstrapVue from 'bootstrap-vue';

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { far } from '@fortawesome/pro-regular-svg-icons';
import { fas } from '@fortawesome/pro-solid-svg-icons';
import { FontAwesomeIcon, FontAwesomeLayers } from '@fortawesome/vue-fontawesome';

// Views
import App from './app.vue';

// Pages
import HomePage from './pages/home.vue';

// HTML
import './index.html';

// Site Style
import './scss/theme.scss';

// IPC
// import * as ipcUtil from './utils/ipc';

// ---------------------------------------------------------------------------------------------------------------------
// Font Awesome
// ---------------------------------------------------------------------------------------------------------------------

library.add(fab, far, fas);
Vue.component('Fa', FontAwesomeIcon);
Vue.component('FaLayers', FontAwesomeLayers);

// ---------------------------------------------------------------------------------------------------------------------
// Bootstrap Vue
// ---------------------------------------------------------------------------------------------------------------------

// import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);

//----------------------------------------------------------------------------------------------------------------------
// Vue Router
//----------------------------------------------------------------------------------------------------------------------

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', name: 'home', component: HomePage }
        // { path: '/about', name: 'about', component: AboutPage },
        // { path: '/dashboard', name: 'dashboard', component: DashboardPage },
        // { path: '/characters/:id', name: 'character', component: CharacterPage },
        // { path: '/settings', name: 'settings', component: SettingsPage }
    ]
});

//----------------------------------------------------------------------------------------------------------------------
// Setup Vue App
//----------------------------------------------------------------------------------------------------------------------

new Vue({
    components: { App },
    router,
    el: '#app',
    template: '<App/>'
});

// ipcUtil.request('test', 'hello', 'Bitches')
//     .then((resp) =>
//     {
//         console.log('response:', resp);
//     });

// ---------------------------------------------------------------------------------------------------------------------
